// console.log("Hello World")

//Arihmetic operators

let x = 1397;
let y = 7831;

//let sum = 1397 + 7831;
let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

// modulus (%)
// gets the remainder from 2 divided values.
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Assignment operator (=)

// Basic Assignment
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber)

//shorthand method for assignment operator
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber)

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber)

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber)

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber)

//multiple operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " +mdas);

let pmdas = 1 + (2 - 3) * (4 / 5);
/*
	4/5 = 0.8
	2-3 = -1
	-1*0.8 = -0.8
	1 + -0.8 = .2
*/
console.log("Result of pemdas operation: " +pmdas);

pemdas = (1 +(2-3)) * (4/5);
/*
	4/5 = 0.8
	2-3 = -1
	1+1 = 0
	0*0.8 = 0
*/
console.log("Result of second pemdas operation: " +pemdas)

//Incrementation vs decrementation
// incrementation (++)
// decrementation (--)

// ++z added 1 to its current value
let z = 1; 
// pre-incrementation
let increment = ++z;
console.log("Result of pre-incrementation operation: " +increment)
console.log("Current Value of z after pre-incrementation: " +z)

// post-incrementation
increment = z++;
console.log("Result of post-incrementation operation: " +increment)
console.log("Current Value of z after post-incrementation: " +z)

// decrementation
//pre-decrementation
let decrement = --z;
console.log("Result of pre-deccrementation operation: " +decrement)
console.log("Value of z after pre-deccrementation operation: " +z)

// post-decrementation
decrement = z--;
console.log("Result of post-deccrementation operation: " +decrement)
console.log("Value of z after post-deccrementation operation: " +z)

// type coercion
// recognized as string because of ""
let numA = "10";
// integer
let numB = 12;
// coercion
let coercion = numA + numB;
// result should be "10"12 or 1012
console.log(coercion);
console.log(typeof coercion);

// non-coercion
let numC= 16;
let numD = 14;
let nonCoercion = numC + numD
console.log(nonCoercion)
console.log(typeof nonCoercion)

// boolean + number
// false = 0
let numE = false +1;
console.log(numE);

// true = 1
let numF = true +1;
console.log(numF);

// Comparison Operators
let juan = "juan";

// Equality Operator (==)
// Checks 2 operands if they are equal/have the same content
// May return boolean value

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == "1");
console.log("juan" == "juan");
console.log("juan" == juan);

//Inequality Operator (!=)
// ! -> not

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log("juan" != "juan");
console.log("juan" != juan);
console.log(0 != false);

// Strict equality operator (===)
console.log(1 === 1);
console.log(1 === 2);
console.log("juan" === juan);

//Strict Inequality (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log("juan" !== juan);

// Relational Operator
let a = 50;
let b = 65;

// GT (>) Greater than operator
let isGreaterThan = a > b;
console.log("isGreaterThan: "+ isGreaterThan);
// LT (<) Less than operator
let isLessThan = a < b;
console.log("isLessThan: "+ isLessThan)

//GTE (>=) Greater than or equal
let isGTorEqual = a >= b;
console.log("isGTorEqual "+ isGTorEqual);

//LET (<=) Less than or equal
let isLTorEqual = a <= b;
console.log("isLTorEqual: " +isLTorEqual)

let numStr = "30";
console.log(a > numStr);

let str = "twenty";
console.log(b >= str)

//In some events, we can receive NaN 
// Nan -> not a number

//Logical Operators 
let isLegalAge = true;
let isRegistered = false;

// logical AND operator (&&)
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: "+allRequirementsMet);

// Logical Or operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: "+someRequirementsMet);

// logical Not operator (!)
// returns opposite value

let someRequiremensNotMet = !isRegistered;
console.log("Result of logical NOT operator: "+someRequiremensNotMet)